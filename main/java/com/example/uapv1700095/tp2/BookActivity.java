package com.example.uapv1700095.tp2;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        final Book book = null;
        BookDbHelper livre = new BookDbHelper(this);

        final EditText Nom = (findViewById(R.id.nameBook));
        final EditText Auteur = (findViewById(R.id.editAuthors));
        final EditText Annee = (findViewById(R.id.editYear));
        final EditText Genre = (findViewById(R.id.editGenres));
        final EditText Publier = (findViewById(R.id.editPublisher));
        final Button b = (Button) findViewById(R.id.boutton);


        //get id Book sélectionné dans l'item
        final Intent intent = getIntent();

        if (intent.getStringExtra("book") == null) {
            book.setTitle(Nom.getText().toString());
            book.setAuthors(Auteur.getText().toString());
            book.setYear(Annee.getText().toString());
            book.setGenres(Genre.getText().toString());
            book.setPublisher(Publier.getText().toString());

            BookDbHelper data = new BookDbHelper(BookActivity.this);
            data.updateBook(book);
        }
        else {
            book.setTitle(Nom.getText().toString());
            book.setAuthors(Auteur.getText().toString());
            book.setYear(Annee.getText().toString());
            book.setGenres(Genre.getText().toString());
            book.setPublisher(Publier.getText().toString());

            BookDbHelper data = new BookDbHelper(BookActivity.this);
            data.addBook(book);
        }

        Button save = findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





        //Bundle bun = intent.getBundleExtra("in");
        //final int id = bun.getInt("var_id");
        //Nom.setText();

        /*
        Cursor cursr = livre.getData(id);
        cursr.moveToFirst();
        String n = cursr.getString(cursr.getColumnIndex(livre.COLUMN_BOOK_TITLE));
        String au = cursr.getString(cursr.getColumnIndex(livre.COLUMN_AUTHORS));
        String a = cursr.getString(cursr.getColumnIndex(livre.COLUMN_YEAR));
        String g =  cursr.getString(cursr.getColumnIndex(livre.COLUMN_GENRES));
        String p = cursr.getString(cursr.getColumnIndex(livre.COLUMN_PUBLISHER));
        */
    }

    /*
    public void showMessage (String title, String message){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
    */
});}}